// NODE.JS ROUTING WITH HTTP METHODS

let http = require("http");


let port = 4000


http.createServer(function (request, response) {

	if (request.url == "http://localhost:4000/" && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Booking System');
	}

	if (request.url == "http://localhost:4000/profile" && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile!');
	}

	if (request.url == "http://localhost:4000/courses" && request.method == "POST") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Here's our course resources");
	}

	if (request.url == "http://localhost:4000/addcourse" && request.method == "PATCH") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Add a course to our resources');
	}

	if (request.url == "http://localhost:4000/updatecourse" && request.method == "PUT") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Update a course to our resources');
	}

	if (request.url == "http://localhost:4000/archivecourses" && request.method == "DELETE") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Archive Courses to Our Resources');
	}

}).listen(port)

console.log('Server running at localhost:4000');